# Borrow controller
class BorrowsController < ApplicationController
  load_and_authorize_resource

  def index
    @borrows = Borrow.all
  end

  def show
    @borrow = Borrow.find(params[:id])
    @user = User.find(@borrow.user_id)
    @book = Book.find(Copy.find(@borrow.copy_id).book_id)
  end

  def new
    @borrow = Borrow.new
  end

  def create
    @borrow = Borrow.new(borrow_params)
    if @borrow.save
      flash[:success] = 'Book issued!'
      Copy.find(@borrow.copy_id).update(status: 1)
      redirect_to @borrow
    else
      render :new
    end
  end

  def destroy
    @borrow = Borrow.find(params[:id])
    Copy.find(@borrow.copy_id).update(status: 0)
    flash[:success] = 'Book returned!'
    @borrow.destroy

    redirect_to borrows_path
  end

  private

  def borrow_params
    params.require(:borrow).permit(:user_id, :copy_id, :due_date)
  end
end
