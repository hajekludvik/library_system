# authors_books controller
class AuthorsBooksController < ApplicationController
  load_and_authorize_resource

  def new
    @author_book = AuthorsBook.new
    @author = params[:author]
    @book = params[:book]
    @authors = Author.all
    @books = Book.all
  end

  def create
    @author_book = AuthorsBook.new(author_book_params)
    if @author_book.save
      flash[:success] = 'Author & book realation created!'
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    @author_book = AuthorsBook.where(author_book_params)
    flash[:success] = 'Author & book realation deleted!'
    @author_book.destroy

    redirect_to root_path
  end

  private

  def author_book_params
    params.require(:authors_book).permit(:author_id, :book_id)
  end
end
