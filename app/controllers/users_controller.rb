# User controller
class UsersController < ApplicationController
  load_and_authorize_resource except: :create

  def index
    @users = User.accessible_by(current_ability, :read).order(:login)
  end

  def show
    @user = User.find(params[:id])
    @reservations = Reservation.where(user_id: @user.id)
    @borrows = Borrow.where(user_id: @user.id)
    @borrow = Borrow.new
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(users_params)
    if @user.save
      flash[:success] = 'Account registered!'
      redirect_to root_path
    else
      render :new
    end
    # authorize! :create, @user
  end

  private

  def users_params
    params.require(:user).permit(:login, :password, :password_confirmation)
  end
end
