# Reservation controller
class ReservationsController < ApplicationController
  load_and_authorize_resource

  def create
    @book = Book.find(params[:book_id])
    @reservation = @book.reservations.create(reservation_params)
    flash[:success] = 'Reservation created!'
    redirect_to book_path(@book)
  end

  def destroy
    @book = Book.find(params[:book_id])
    @reservation = @book.reservations.find(params[:id])
    @user = User.find(@reservation.user_id)
    @reservation.destroy
    flash[:success] = 'Reservation cancelled!'
    redirect_to user_path(@user)
  end

  private

  def reservation_params
    params.require(:reservation).permit(:book_id, :user_id)
  end
end
