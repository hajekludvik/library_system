# Author controller
class AuthorsController < ApplicationController
  load_and_authorize_resource

  def index
    @authors = Author.order(:name)
  end

  def show
    @author = Author.find(params[:id])
    @books = @author.books.order(:title)
    client = Goodreads.new(Goodreads.configuration)
    @grauthor = client.author(@author.goodreads_id)
  end

  def new
    @author = Author.new
    @grauthor = params[:grauthor]
  end

  def edit
    @author = Author.find(params[:id])
  end

  def create
    @author = Author.new(author_params)

    if @author.save
      flash[:success] = 'Author added!'
      redirect_to @author
    else
      render 'new'
    end
  end

  def update
    @author = Author.find(params[:id])

    if @author.update(author_params)
      flash[:success] = 'Author updated!'
      redirect_to @author
    else
      render 'edit'
    end
  end

  def destroy
    @author = Author.find(params[:id])
    @author.destroy
    flash[:success] = 'Author deleted!'
    redirect_to authors_path
  end

  def handle_search
    begin
      client = Goodreads.new(Goodreads.configuration)
      @grauthor = client.author_by_name(params[:name])
    rescue Goodreads::NotFound
      @grauthor = { name: 'no author found' }
    end
    flash[:success] = @grauthor[:name]
    redirect_to new_author_path(grauthor: @grauthor)
  end

  private

  def author_params
    params.require(:author).permit(:name, :goodreads_id)
  end
end
