# Admin controller
class AdminsController < ApplicationController
  load_and_authorize_resource except: :create

  def index
    @admins = Admin.order(:login)
  end

  def show
    @admin = Admin.find(params[:id])
  end

  def new
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(admins_params)
    if @admin.save_without_session_maintenance
      flash[:success] = 'Admin  added!'
      redirect_to admins_path
    else
      render :new
    end
    authorize! :create, @admin
  end

  def destroy
    @admin = Admin.find(params[:id])
    @admin.destroy
    flash[:success] = 'Admin deleted!'
    redirect_to admins_path
  end

  private

  def admins_params
    params.require(:admin).permit(:login, :password, :password_confirmation)
  end
end
