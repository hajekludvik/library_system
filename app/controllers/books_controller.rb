# Book controller
class BooksController < ApplicationController
  load_and_authorize_resource

  def index
    @books = Book.order(:title)
  end

  def show
    @book = Book.find(params[:id])
    @authors = @book.authors.order(:name)
    @copies = Copy.where(book_id: @book.id)
    @num_of_copies = @copies.count
    @num_of_free_copies = @copies.where(status: 0).count
    @user = current_user
    @borrow = Borrow.new
    @users = User.order(:login)
    client = Goodreads.new(Goodreads.configuration)
    @grbook = client.book(@book.goodreads_id)
  end

  def new
    @book = Book.new
    @isbn = params[:isbn]
    @gr_id = params[:gr_id]
    @title = params[:title]
    client = Goodreads.new(Goodreads.configuration)
    @grbook = client.book(@gr_id)
  end

  def edit
    @book = Book.find(params[:id])
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      create_relations_with_authors(@book)
      flash[:success] = 'Book added!'
      redirect_to @book
    else
      render 'new'
    end
  end

  def update
    @book = Book.find(params[:id])

    if @book.update(book_params)
      flash[:success] = 'Book updated!'
      redirect_to @book
    else
      render 'edit'
    end
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    flash[:success] = 'Book deleted!'
    redirect_to books_path
  end

  def handle_search
    begin
      client = Goodreads.new(Goodreads.configuration)
      @grbook = client.book_by_title(params[:title])
      @isbn = @grbook.isbn
      @gr_id = @grbook.id
      @title = @grbook.title
    rescue Goodreads::NotFound
      @title = 'no book found'
    end
    flash[:success] = @title
    redirect_to new_book_path(isbn: @isbn, gr_id: @gr_id, title: @title)
  end

  private

  def book_params
    params.require(:book).permit(:goodreads_id, :title, :isbn)
  end

  def create_relations_with_authors(book)
    @book = book
    client = Goodreads.new(Goodreads.configuration)
    @grbook = client.book(@book.goodreads_id)
    if @grbook.authors.author.is_a? Array # if more authors
      @grbook.authors.author.each do |author|
        create_author(author, @book)
      end
    else # one author
      create_author(@grbook.authors.author, @book)
    end
  end

  def create_author(grauthor, book)
    @grauthor = grauthor
    @book = book
    @author = Author.where(goodreads_id: @grauthor.id).first
    if @author.nil? # author not in db
      @author = Author.create(name: @grauthor.name, goodreads_id: @grauthor.id)
    end
    AuthorsBook.create(author_id: @author.id, book_id: @book.id)
  end
end
