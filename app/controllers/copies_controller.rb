# Copy controller
class CopiesController < ApplicationController
  load_and_authorize_resource

  def show
  end

  def create
    @book = Book.find(params[:book_id])
    @copy = @book.copies.create(copy_params)
    flash[:success] = 'Copy created!'
    redirect_to edit_book_path(@book)
  end

  def destroy
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    @copy.destroy
    flash[:success] = 'Copy deleted!'
    redirect_to edit_book_path(@book)
  end

  private

  def copy_params
    params.require(:copy).permit(:status)
  end
end
