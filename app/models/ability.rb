# Used by CanCanCan to initialize user rights.
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.role == 'sysadmin'
      can :manage, :all
      cannot :create, Reservation
    elsif user.role == 'admin'
      can :manage, :all
      cannot [:create, :update, :destroy], Admin
      cannot :create, Reservation
    elsif user.role == 'user'
      can :read, [Book, Author, Copy]
      can :read, User, id: user.id
      can :read, Borrow, user_id: user.id
      can :create, Reservation
      can :manage, Reservation, user_id: user.id
    else # guest
      can :read, [Book, Author, Copy]
      can :create, User
    end
  end
end
