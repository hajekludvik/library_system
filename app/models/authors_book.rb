# AuthorsBook model
class AuthorsBook < ApplicationRecord
  validates :author_id, presence: true
  validates :book_id, presence: true
end
