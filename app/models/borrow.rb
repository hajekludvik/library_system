# Borrow model
class Borrow < ApplicationRecord
  belongs_to :user
  belongs_to :copy
  validates :user_id, presence: true
  validates :copy_id, presence: true
  validates :due_date, presence: true
end
