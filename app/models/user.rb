# Users used with Authlogic.
# Specifies encryption protocol.
# role used for authorizating access by CanCanCan.
class User < ApplicationRecord
  has_many :reservations
  has_many :borrows

  acts_as_authentic do |c|
    c.crypto_provider = Authlogic::CryptoProviders::Sha512
  end

  def role
    if !current_user.nil?
      'user'
    elsif !current_admin.nil?
      if current_admin.login == 'admin'
        'sysadmin'
      else
        'admin'
      end
    else
      'guest'
    end
  end
end
