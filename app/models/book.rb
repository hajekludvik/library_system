# Book model
class Book < ApplicationRecord
  has_and_belongs_to_many :authors
  has_many :copies, dependent: :destroy
  has_many :reservations, dependent: :destroy
  validates :title, presence: true
  validates :isbn, presence: true
  validates :goodreads_id, presence: true
end
