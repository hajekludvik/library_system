# Copy model
class Copy < ApplicationRecord
  belongs_to :book
  has_one :borrow, dependent: :destroy
  validates :status, presence: true
  validates :book_id, presence: true
end
