# Screw you, rubocop. There is a naming convention for migrations.
class RemoveYearPublishedFromBooks < ActiveRecord::Migration[5.0]
  def change
    remove_column :books, :year_published, :date
  end
end
