# Screw you, rubocop. There is a naming convention for migrations.
class RemoveAuthorIdFromBook < ActiveRecord::Migration[5.0]
  def change
    remove_column :books, :author_id, :integer
  end
end
