# Screw you, rubocop. There is a naming convention for migrations.
class RemovePublishedFromBooks < ActiveRecord::Migration[5.0]
  def change
    remove_column :books, :publisher, :string
  end
end
