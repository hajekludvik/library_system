# Screw you, rubocop. There is a naming convention for migrations.
class RemoveIssuedCopiesFromBook < ActiveRecord::Migration[5.0]
  def change
    remove_column :books, :issued_copies, :integer
  end
end
