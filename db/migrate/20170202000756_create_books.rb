# Screw you, rubocop. There is a naming convention for migrations.
class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.references :author, foreign_key: true
      t.string :title
      t.string :publisher
      t.date :year_published
      t.integer :num_of_copies
      t.integer :issued_copies

      t.timestamps
    end
  end
end
