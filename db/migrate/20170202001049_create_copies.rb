# Screw you, rubocop. There is a naming convention for migrations.
class CreateCopies < ActiveRecord::Migration[5.0]
  def change
    create_table :copies do |t|
      t.references :book, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end
