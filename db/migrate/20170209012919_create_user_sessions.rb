# Screw you, rubocop. There is a naming convention for migrations.
class CreateUserSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_sessions do |t|
      t.timestamps
    end
  end
end
