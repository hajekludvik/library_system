# Screw you, rubocop. There is a naming convention for migrations.
class RemoveFirstnameFromAuthor < ActiveRecord::Migration[5.0]
  def change
    remove_column :authors, :firstname, :string
  end
end
