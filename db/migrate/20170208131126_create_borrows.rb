# Screw you, rubocop. There is a naming convention for migrations.
class CreateBorrows < ActiveRecord::Migration[5.0]
  def change
    create_table :borrows do |t|
      t.references :user, foreign_key: true
      t.references :copy, foreign_key: true
      t.date :due_date

      t.timestamps
    end
  end
end
