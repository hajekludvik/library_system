# Screw you, rubocop. There is a naming convention for migrations.
class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      t.references :user, foreign_key: true
      t.references :copy, foreign_key: true

      t.timestamps
    end
  end
end
