# Screw you, rubocop. There is a naming convention for migrations.
class RemoveNumOfCopiesFromBook < ActiveRecord::Migration[5.0]
  def change
    remove_column :books, :num_of_copies, :integer
  end
end
