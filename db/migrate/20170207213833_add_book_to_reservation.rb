# Screw you, rubocop. There is a naming convention for migrations.
class AddBookToReservation < ActiveRecord::Migration[5.0]
  def change
    add_reference :reservations, :book, foreign_key: true
  end
end
