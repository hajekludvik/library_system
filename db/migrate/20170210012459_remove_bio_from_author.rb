# Screw you, rubocop. There is a naming convention for migrations.
class RemoveBioFromAuthor < ActiveRecord::Migration[5.0]
  def change
    remove_column :authors, :bio, :text
  end
end
