# Screw you, rubocop. There is a naming convention for migrations.
class AddIsbnGoodreadsIdToBook < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :isbn, :string
    add_column :books, :goodreads_id, :string
  end
end
