# Screw you, rubocop. There is a naming convention for migrations.
class RemoveCopyFromReservation < ActiveRecord::Migration[5.0]
  def change
    remove_column :reservations, :copy_id, :integer
  end
end
