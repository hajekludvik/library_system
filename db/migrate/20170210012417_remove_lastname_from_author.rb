# Screw you, rubocop. There is a naming convention for migrations.
class RemoveLastnameFromAuthor < ActiveRecord::Migration[5.0]
  def change
    remove_column :authors, :lastname, :string
  end
end
