Rails.application.routes.draw do
  resources :authors do
    collection do
      get 'search'
      get 'handle_search'
    end
  end
  resources :books do
    resources :copies
    resources :reservations
    collection do
      get 'search'
      get 'handle_search'
    end
  end

  resources :authors_books
  resources :borrows

  resources :users
  resources :user_sessions, only: [:create, :destroy]
  delete '/sign_out', to: 'user_sessions#destroy', as: :sign_out
  get '/sign_in', to: 'user_sessions#new', as: :sign_in

  resources :admins
  resources :admin_sessions, only: [:create, :destroy]
  delete '/admin_sign_out', to: 'admin_sessions#destroy', as: :admin_sign_out
  get '/admin_sign_in', to: 'admin_sessions#new', as: :admin_sign_in

  root 'welcome#index'
end
