# README

This is a repository for simple Library management system that was created as a final project for Ruby programing class (PV249) at Faculty of Informatics of Masaryk University in Brno for semestr Fall 2016.

Models:

* author
* book
* copy
* reservation
* borrow
* user
* admin

Functinality:

* four roles: guest, user, admin, sysadmin

  - guest can browse books and authors and sign up to become user
  - user can browse books and authors, manage his reservations of books and see books issued to him
  - admin can manage books, authors, users, reservations (except creating) and handle issuing books and browse admins
  - sysadmin can do anything (same rights as admin + admin management)

* information about books and authors gathered from [GoodReads](https://www.goodreads.com/)

Created with Rails version 5.0.1
Additional dependencies: gems authlogic, cancancan, goodreads

After creating a fork you have to db:seed to create the sysadmin.
The database is empty, waiting for you to populate.
